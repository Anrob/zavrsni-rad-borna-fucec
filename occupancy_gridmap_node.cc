#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <grid_map_ros/grid_map_ros.hpp>
#include <grid_map_msgs/GridMap.h>
#include <geometry_msgs/PointStamped.h>
#include <sensor_msgs/LaserScan.h>
#include <tf2_ros/transform_listener.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <cmath>

using namespace grid_map;

tf2_ros::Buffer *tfBuffer;
tf2_ros::TransformListener *tfListener;
GridMap *map;
ros::Publisher publisher, pub_scan, filter_scan;
double offset_low = 0.2, offset_high = 3.0;
double phi_low = -31.;
double phi_high = 31.;
double th_low = -180.;
double th_high = 180.;
double th_out_low = 180.;
double th_out_high = -180.;
double phi_out_low = 31;
double phi_out_high = -31;
double filter_x_min = 5;
double filter_x_max = 4;
double filter_y_min = 2;
double filter_y_max = 1;
int mapwidth = 200, mapheight = 2000;
float resolution = 0.5;
int cyclenum = 0;

struct Point
{
  float x;
  float y;
  float z;
};

float bytesToFloat(uchar b0, uchar b1, uchar b2, uchar b3)
{
  float f;
  uchar b[] = {b0, b1, b2, b3};
  memcpy(&f, &b, sizeof(f));
  return f;
}

bool use_point(const float new_value, const float old_value, const float range_min, const float range_max)
{
  // Check for NaNs and Infs, a real number within our limits is more desirable than these.
  bool new_finite = std::isfinite(new_value);
  bool old_finite = std::isfinite(old_value);

  // Infs are preferable over NaNs (more information)
  if (!new_finite && !old_finite)
  { // Both are not NaN or Inf.
    if (!std::isnan(new_value))
    { // new is not NaN, so use it's +-Inf value.
      return true;
    }
    return false; // Do not replace old_value
  }

  // If not in range, don't bother
  bool range_check = range_min <= new_value && new_value <= range_max;
  if (!range_check)
  {
    return false;
  }

  if (!old_finite)
  { // New value is in range and finite, use it.
    return true;
  } 

  // Finally, if they are both numerical and new_value is closer than old_value, use new_value.
  bool shorter_check = new_value < old_value;
  return shorter_check;
}

void velodyneCallback(const sensor_msgs::PointCloud2::ConstPtr &msg)
{

  if (msg != NULL)
  {
    auto data = msg->data;
    int width = msg->width; // width is number of messages
    int pnt_stp = msg->point_step;
    int n_pnts = width; // /pnt_stp;
    ROS_INFO("height %i", msg->height);
    ROS_INFO("width %i", width);
    ROS_INFO("point_step %i", pnt_stp);
    ROS_INFO("row_step %i", msg->row_step);
    ROS_INFO("data.size() %i", msg->data.size());
    ROS_INFO("point_step * width %i", pnt_stp * width);
    ROS_INFO("n_pnts %i", n_pnts);

    GridMap *localmap;
    localmap = new GridMap({"elevation"});
    localmap->setFrameId("map");
    localmap->setGeometry(Length(mapwidth, mapheight), resolution);

    Point pnts[n_pnts];
    sensor_msgs::LaserScan scan_msg;
    scan_msg.header = msg->header;
    scan_msg.header.frame_id = "laserscan";
    scan_msg.angle_min = -M_PI;
    scan_msg.angle_max = M_PI;
    scan_msg.angle_increment = (scan_msg.angle_max - scan_msg.angle_min) / (720);
    scan_msg.time_increment = 0.0;
    scan_msg.scan_time = 0.1;
    scan_msg.range_min = 0;
    scan_msg.range_max = 30.;
    uint32_t ranges_size = 721;
    scan_msg.ranges.assign(ranges_size, std::numeric_limits<float>::quiet_NaN());
    double r, th, phi;
    int index;
    double minz = 100., maxz = 0.;
    double minphi = 100., maxphi = -100.;
    double minth = 100., maxth = -100.;

    geometry_msgs::TransformStamped transformStamped, transformStamped2, transformStamped3, transformStamped4;
    try
    {
      transformStamped = tfBuffer->lookupTransform("map", "lidar", ros::Time(0));
      transformStamped2 = tfBuffer->lookupTransform("laserscan", "lidar", ros::Time(0));
      transformStamped3 = tfBuffer->lookupTransform("map", "base_link", ros::Time(0));
      transformStamped4 = tfBuffer->lookupTransform("base_link", "lidar", ros::Time(0));
    }
    catch (tf2::TransformException &ex)
    {
      ROS_WARN("%s", ex.what());
      ros::Duration(1.0).sleep();
    }

    geometry_msgs::Point temp_pt, height_pt;
    temp_pt.x = 0;
    temp_pt.y = 0;
    temp_pt.z = 0;
    tf2::doTransform(temp_pt, height_pt, transformStamped3);
    ROS_INFO("height base_link %f", height_pt.z);
    // height_pt.z=0;
    ros::WallTime start_, end_;

    start_ = ros::WallTime::now();
    std::vector<Index> indlocal;
    indlocal.clear();

    for (uint i = 0; i < n_pnts; i++)
    {

      int ofst = i * pnt_stp;
      pnts[i].x = bytesToFloat(data[ofst + 0], data[ofst + 1], data[ofst + 2], data[ofst + 3]);
      pnts[i].y = bytesToFloat(data[ofst + 4], data[ofst + 5], data[ofst + 6], data[ofst + 7]);
      pnts[i].z = bytesToFloat(data[ofst + 8], data[ofst + 9], data[ofst + 10], data[ofst + 11]);

      geometry_msgs::Point transformed_pt, local_pt, baselink_pt;
      geometry_msgs::Point initial_pt;

      initial_pt.x = pnts[i].x;
      initial_pt.y = pnts[i].y;
      initial_pt.z = pnts[i].z;

      // Transform the data to the map frame so we can add elevations to the map
      tf2::doTransform(initial_pt, transformed_pt, transformStamped);
      tf2::doTransform(initial_pt, local_pt, transformStamped2);
      tf2::doTransform(initial_pt, baselink_pt, transformStamped4);
      Position position;
      position.x() = transformed_pt.x;
      position.y() = transformed_pt.y;
      // ROS_INFO("%f %f", position.x(), position.y());

      // Track the highest and lowest point
      if (baselink_pt.z > maxz)
        maxz = baselink_pt.z;
      if (baselink_pt.z < minz)
        minz = baselink_pt.z;

      r = sqrt(pow(initial_pt.x, 2.0) + pow(initial_pt.y, 2.0));
      th = atan2(initial_pt.y, initial_pt.x);
      phi = M_PI / 2. - atan2(r, initial_pt.z);

      if (th > maxth)
        maxth = th;
      if (th < minth)
        minth = th;
      if (phi > maxphi)
        maxphi = phi;
      if (phi < minphi)
        minphi = phi;
      Index ind;
      if (r<30)
      if (localmap->getIndex(position, ind))
      {
        bool xy_filter_condition = (baselink_pt.x >= filter_x_min) && (baselink_pt.x <= filter_x_max) && (baselink_pt.y >= filter_y_min) && (baselink_pt.y <= filter_y_max);
        bool angle_condition = (th >= th_low) && (th <= th_high);
        bool angle_condition_filter_out = (th >= th_out_low) && (th <= th_out_high) && (phi >= phi_out_low) && (phi <= phi_out_high);
        if (th_low > th_high)
          angle_condition = (th >= th_low) || (th <= th_high);
        if (angle_condition && (phi >= phi_low) && (phi <= phi_high) && (!angle_condition_filter_out) && (!xy_filter_condition))
        {

          if ((float)localmap->at("elevation", ind) == localmap->at("elevation", ind))
          {
            if (transformed_pt.z - height_pt.z > localmap->at("elevation", ind))
              localmap->at("elevation", ind) = transformed_pt.z - height_pt.z;
          }
          else
          {
            localmap->at("elevation", ind) = transformed_pt.z - height_pt.z;
            indlocal.push_back(ind);
          }
          // ROS_INFO("%f %f %f %f", position.x(), position.y(), localmap->at("elevation", ind), transformed_pt.z);
          //            if ((float) map->at("elevation", ind)==map->at("elevation", ind)){
          //            if (transformed_pt.z > map->at("elevation", ind))
          //            	map->at("elevation", ind) = transformed_pt.z;
          //						}else{
          //							map->at("elevation", ind) = transformed_pt.z;
          //						}
          // Set height constraints according to vehicle dimensions - for Husky -0.5 and 1.0 m are fine
          // also control for the actual viewing angles
          if ((transformed_pt.z - height_pt.z > offset_low) && (transformed_pt.z - height_pt.z < offset_high))
          {
            r = sqrt(pow(local_pt.x, 2.0) + pow(local_pt.y, 2.0));
            th = atan2(local_pt.y, local_pt.x);
            index = (th - scan_msg.angle_min) / scan_msg.angle_increment;
            // ROS_INFO("%f %f %f", initial_pt.x, initial_pt.y, initial_pt.z);
            if (use_point(r, scan_msg.ranges[index], scan_msg.range_min, scan_msg.range_max))
            {
              scan_msg.ranges[index] = r;
            }
          }
        }
      }
    }
    end_ = ros::WallTime::now();
    double execution_timepart = (end_ - start_).toNSec() * 1e-6;
    ROS_INFO_STREAM("Exectution time before rewriting to global map (ms): " << execution_timepart);

    std::vector<Index>::iterator it;
    for (it = indlocal.begin(); it != indlocal.end(); it++)
    {
      map->at("elevation", *it) = localmap->at("elevation", *it);
    }

    // for (GridMapIterator iterator(*localmap);!iterator.isPastEnd(); ++iterator) {
    // 	if ((float) localmap->at("elevation", *iterator)==localmap->at("elevation", *iterator)){
    // 		map->at("elevation", *iterator) = localmap->at("elevation", *iterator);
    // 	}
    // }

    delete localmap;

    end_ = ros::WallTime::now();
    double execution_timepart2 = (end_ - start_).toNSec() * 1e-6;
    ROS_INFO_STREAM("Execution time after rewriting to global map (ms): " << execution_timepart2);

    // Publish grid map.
    cyclenum++;
    if (cyclenum % 5 == 0)
    {
      cyclenum = 0;
      ros::Time time = ros::Time::now();
      map->setTimestamp(time.toNSec());
      grid_map_msgs::GridMap message;

      GridMapRosConverter::toMessage(*map, message);
      publisher.publish(message);
      ROS_INFO_THROTTLE(1.0, "Grid map (timestamp %f) published.", message.info.header.stamp.toSec());
    }

    ROS_INFO("cyclenum %d minz maxz %f %f m, minth maxth %f %f deg, minphi maxphi %f %f deg", cyclenum, minz, maxz, minth * 180. / M_PI, maxth * 180. / M_PI, minphi * 180. / M_PI, maxphi * 180. / M_PI);
    //publish scan
    pub_scan.publish(scan_msg);

    end_ = ros::WallTime::now();

    // print results
    double execution_time = (end_ - start_).toNSec() * 1e-6;
    ROS_INFO_STREAM("Execution time (ms): " << execution_time);
  }
}

void dust_filter(const sensor_msgs::LaserScan msg){
  
  sensor_msgs::LaserScan filter_msg = msg;
  //filter_msg.header.frame_id = "dust_filtered";
  int num;

  //(..., nan, nan, nan, 5.2328931982, nan, nan, nan, ...) -> pogresno je ocitana prepreka
  for (int i = 0; i < 720; i++){
    num = std::isnan(filter_msg.ranges[i]); //if num = 1 -> ranges[i] = nan

    //SREDNJA OD 3 VRIJEDNOSTI JE BROJ, OSTALE SU NAN
    /*if (i > 0 && i < 719){
      if (std::isnan(filter_msg.ranges[i-1])==1 && num != 1 && std::isnan(filter_msg.ranges[i+1])==1)
        filter_msg.ranges[i] = std::numeric_limits<float>::quiet_NaN();
    }*/

    //SREDNJA OD 5 VRIJEDNOSTI JE BROJ, OSTALE SU NAN
    if (i > 1 && i < 718){
      if (std::isnan(filter_msg.ranges[i-2])==1 && std::isnan(filter_msg.ranges[i-1])==1 && num != 1 && std::isnan(filter_msg.ranges[i+1])==1 && std::isnan(filter_msg.ranges[i+2])==1)
        filter_msg.ranges[i] = std::numeric_limits<float>::quiet_NaN();
    }
  }
  filter_scan.publish(filter_msg);
  //pub_scan.publish(filter_msg);
}

int main(int argc, char **argv)
{
  // Set gridmap params
  // Initialize node and publisher.
  ros::init(argc, argv, "grid_map_simple_demo");
  ros::NodeHandle nh("~");
  publisher = nh.advertise<grid_map_msgs::GridMap>("grid_mapbag", 1, true);
  pub_scan = nh.advertise<sensor_msgs::LaserScan>("scanbag", 1, true);
  
  //publisher i subscriber na filter
  ros::Subscriber filtersub = nh.subscribe("/occmap/scanbag", 1000, dust_filter);
  filter_scan = nh.advertise<sensor_msgs::LaserScan>("filteredbag", 1, true);
  //pub_scan = nh.advertise<sensor_msgs::LaserScan>("filteredbag", 1, true);
  

  nh.getParam("/occmap/map_width", mapwidth);
  nh.getParam("/occmap/map_height", mapheight);
  nh.getParam("/occmap/resolution", resolution);
  nh.getParam("/occmap/offset_low", offset_low);
  nh.getParam("/occmap/offset_high", offset_high);
  nh.getParam("/occmap/angle_z_low", th_low);
  nh.getParam("/occmap/angle_z_high", th_high);
  nh.getParam("/occmap/angle_z_out_low", th_out_low);
  nh.getParam("/occmap/angle_z_out_high", th_out_high);
  nh.getParam("/occmap/angle_xy_low", phi_low);
  nh.getParam("/occmap/angle_xy_high", phi_high);
  nh.getParam("/occmap/angle_xy_out_low", phi_out_low);
  nh.getParam("/occmap/angle_xy_out_high", phi_out_high);
  nh.getParam("/occmap/filter_x_min", filter_x_min);
  nh.getParam("/occmap/filter_y_min", filter_y_min);
  nh.getParam("/occmap/filter_x_max", filter_x_max);
  nh.getParam("/occmap/filter_y_max", filter_y_max);
  th_low = th_low * M_PI / 180.;
  th_high = th_high * M_PI / 180.;
  phi_low = phi_low * M_PI / 180.;
  phi_high = phi_high * M_PI / 180.;
  th_out_low = th_out_low * M_PI / 180.;
  th_out_high = th_out_high * M_PI / 180.;
  phi_out_low = phi_out_low * M_PI / 180.;
  phi_out_high = phi_out_high * M_PI / 180.;

  ROS_INFO("Setting the map size to %d m x %d m at the resolution %f", mapwidth, mapheight, resolution);
  ROS_INFO("Setting the low and high offset values for obstacle avoidance: offset_low=%f m, offset_high=%f m", offset_low, offset_high);
  ROS_INFO("Setting the low and high offset angular values for filtering the readings: around z axis: angle_z_low=%f deg, angle_z_high=%f deg (-180 and 180 means no filtering); around xy plane: angle_xy_low=%f, angle_xy_high=%f (-31 and 31 means no filtering)", th_low * 180. / M_PI, th_high * 180. / M_PI, phi_low * 180. / M_PI, phi_high * 180. / M_PI);
  ROS_INFO("Setting the low and high offset angular values for filtering out the readings: around z axis: angle_z_out_low=%f deg, angle_z_out_high=%f deg (180 and -180 means no filtering); around xy plane: angle_xy_out_low=%f, angle_xy_out_high=%f (31 and -31 means no filtering)", th_out_low * 180. / M_PI, th_out_high * 180. / M_PI, phi_out_low * 180. / M_PI, phi_out_high * 180. / M_PI);
  //Initialize tf
  tfBuffer = new tf2_ros::Buffer;
  tfListener = new tf2_ros::TransformListener(*tfBuffer);

  // Work with grid map in a loop.
  ros::Rate rate(10.0);

  map = new GridMap({"elevation"});
  map->setFrameId("map");
  map->setGeometry(Length(mapwidth, mapheight), resolution);

  // Initialize point cloud subscriber
  ros::Subscriber sub = nh.subscribe("/velodyne_points", 1000, velodyneCallback);

  while (ros::ok())
  {
    ros::spinOnce();
    rate.sleep();
  }

  return 0;
}
